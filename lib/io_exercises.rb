# I/O Exercises
#
# * Write a `guessing_game` method. The computer should choose a number between
#   1 and 100. Prompt the user to `guess a number`. Each time through a play loop,
#   get a guess from the user. Print the number guessed and whether it was `too
#   high` or `too low`. Track the number of guesses the player takes. When the
#   player guesses the number, print out what the number was and how many guesses
#   the player needed.
# * Write a program that prompts the user for a file name, reads that file,
#   shuffles the lines, and saves it to the file "{input_name}-shuffled.txt". You
#   could create a random number using the Random class, or you could use the
#   `shuffle` method in array.

def guessing_game
  answer = rand(1..100)
  guess_count = 0

  loop do
    puts "Guess a number..."
    guess = gets.chomp.to_i
    guess_count += 1
    puts "You guessed #{guess}."
    if guess == answer
      puts "You guessed right with #{guess} in #{guess_count} guesses."
      break
    elsif guess > answer
      puts "Guess is too high. Guess again."
    elsif guess < answer
      puts "Guess is too low. Guess again."
    end
  end
end

def file_shuffler(filename)
  write_filename = filename.split(".").first + "--shuffled"
  extension = filename.split(".").last

  File.open("#{write_filename}.#{extension}", "w") do |f|
    File.readlines(filename).shuffle.each do |line|
      f.puts line
    end
  end
end
